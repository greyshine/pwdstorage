package de.greyshine.pwdstorage.backend;


import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Slf4j
class PasswordTests {

    @Test
    void testPropertPasswords() {

        assertFalse(Utils.isProperPassword(null));
        assertFalse(Utils.isProperPassword(""));

        assertFalse(Utils.isProperPassword("1test!"));
        assertFalse(Utils.isProperPassword("1TEST!"));

        assertFalse(Utils.isProperPassword("!tEstx"));
        assertFalse(Utils.isProperPassword("!12345"));

        assertTrue(Utils.isProperPassword("1Test!"));
        assertTrue(Utils.isProperPassword("!1tEst"));

        assertFalse(Utils.isProperPassword("!1tEst "));
        assertFalse(Utils.isProperPassword(" !1tEst"));

        assertFalse(Utils.isProperPassword("!1t Est"));

        assertFalse(Utils.isProperPassword("'1t Est'"));
    }

    @Test
    public void testTestPassword() {

        String password = "User1!";

        String sha256 = Utils.toSha256(password);

        Assertions.assertEquals("2f378f30bbf63d61889492ec38ba7d6716c48357e07a623be8beb67f2a7bf21b", sha256);

        log.info("password:\n{}", sha256);
    }
}
