package de.greyshine.pwdstorage.backend;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.json.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import static de.greyshine.pwdstorage.backend.Utils.isNotBlank;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest(args = "home=./target/junit-test-home/clientRemoteTest")
@AutoConfigureMockMvc
//@TestPropertySource(locations = "classpath:/application-client-intgerationtest.properties")
@Slf4j
public class ClientRemoteTests {

    public static final String USER_PASSWORD = "{\"user\":\"testuser\", \"password\":\"Abc!123\"}";

    private String token;
    private String createdId;

    @Autowired
    private MockMvc mockMvc;

    @BeforeAll
    public static void beforeAll() {

        final File rootDir = new File("target/junit-test-home/clientRemoteTest").getAbsoluteFile();

        final File[] files = rootDir.listFiles();
        log.info("files to delete: {}", files.length);

        Arrays.stream(files).forEach(file -> {
            try {
                FileUtils.forceDelete(file);
            } catch (IOException exception) {
                exception.printStackTrace();
            }
        });

        Assertions.assertEquals(0, rootDir.listFiles().length, "Not all files could be deleted");
    }

    @Test
    @Order(1)
    public void testCreateAccount() throws Exception {

        mockMvc.perform(post("/api/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(USER_PASSWORD)
        )
                .andExpect(status().isOk())
                .andExpect(resultMatcher -> {
                    token = resultMatcher.getResponse().getContentAsString();
                    log.info("token: {}", token);
                });
    }

    @Test
    @Order(2)
    public void testListEntries() throws Exception {

        if (token == null) {
            this.testCreateAccount();
        }

        mockMvc.perform(get("/api/data")
                .contentType(MediaType.APPLICATION_JSON)
                .header("TKN", token))
                .andExpect(status().isOk())
                .andExpect(resultMatcher -> {

                    log.info("{}", resultMatcher);

                    final JSONObject json = new JSONObject(resultMatcher.getResponse().getContentAsString());
                    log.info("{}", json.toString(4));

                    Assertions.assertTrue(0 < json.getJSONArray("entries").length());
                });
    }

    @Test
    @Order(3)
    public void testCreateEntry() throws Exception {

        if (token == null) {
            this.testCreateAccount();
        }

        final JSONObject requestJson = new JSONObject();
        requestJson.put("key", "Entry1");
        requestJson.put("value", "Value\nfor Entry 1");

        mockMvc.perform(post("/api/upsert")
                .contentType(MediaType.APPLICATION_JSON)
                .header("TKN", token)
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestJson.toString())
        )
                .andExpect(status().isOk())
                .andExpect(resultMatcher -> {

                    log.info("{}", resultMatcher);
                    final JSONObject json = new JSONObject(resultMatcher.getResponse().getContentAsString());
                    log.info("{}", json.toString(2));

                    createdId = json.getString("id");
                    Assertions.assertTrue(isNotBlank(createdId));

                    Assertions.assertEquals(requestJson.get("key"), json.getString("key"));
                    Assertions.assertEquals(requestJson.get("value"), json.getString("value"));

                });

        testListEntries();
    }

    @Test
    @Order(4)
    public void testUpdate() throws Exception {

        if (token == null) {
            this.testCreateEntry();
        }

        final JSONObject requestJson = new JSONObject();
        requestJson.put("id", createdId);
        requestJson.put("key", "updated key");
        requestJson.put("value", "updated value");

        mockMvc.perform(post("/api/upsert")
                .contentType(MediaType.APPLICATION_JSON)
                .header("TKN", token)
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestJson.toString())
        )
                .andExpect(status().isOk())
                .andExpect(resultMatcher -> {

                    log.info("{}", resultMatcher);
                    final JSONObject json = new JSONObject(resultMatcher.getResponse().getContentAsString());
                    log.info("{}", json.toString(2));

                    Assertions.assertEquals(createdId, json.getString("id"));
                    Assertions.assertEquals("updated key", json.getString("key"));
                    Assertions.assertEquals("updated value", json.getString("value"));
                });

        testListEntries();
    }

    @Test
    public void testVersion() throws Exception {

        mockMvc.perform(get("/api/version"))
                .andExpect(status().isOk())
                .andDo(rm -> {
                    final var result = rm.getResponse().getContentAsString();
                    log.info("version: {}", result);
                });
    }


}
