package de.greyshine.pwdstorage.backend;


import com.fasterxml.jackson.annotation.JsonFormat;
import de.greyshine.pwdstorage.backend.model.Entry;
import de.greyshine.pwdstorage.backend.model.UserData;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;

@Slf4j
public class JsonTests {

    @Test
    public void test1() throws IOException {

        final LocalDateTime TIME = LocalDateTime.of(2022, 03, 30, 11, 12, 13).plusNanos(321);

        final ByteArrayOutputStream baos = new ByteArrayOutputStream();

        final Item item1 = new Item().ldt(TIME);
        log.info("Item1:\n{}", item1.toString());

        Utils.OBJECT_MAPPER.writeValue(baos, item1);

        log.info(baos.toString(StandardCharsets.UTF_8));

        final Item item2 = Utils.OBJECT_MAPPER.readValue(baos.toByteArray(), Item.class);

        log.info("Item2:\n{}", item2.toString());

        Assertions.assertEquals(item1, item2, "Items not equal");
    }

    @Test
    public void test2() throws IOException {

        final UserData ud1 = new UserData();
        ud1.setUser("test");
        ud1.setCreated(LocalDateTime.now());
        ud1.setUpdated(LocalDateTime.now());
        ud1.version++;

        final Entry entry = new Entry();
        entry.setCreated(LocalDateTime.now());
        entry.setUpdated(LocalDateTime.now());
        entry.setId("ID-1");
        entry.setKey("My-ID");
        entry.setValue("Some data");
        ud1.getEntries().add(entry);

        final ByteArrayOutputStream baos = new ByteArrayOutputStream();

        Utils.OBJECT_MAPPER.writeValue(baos, ud1);

        log.info("Json:\n{}", baos.toString(StandardCharsets.UTF_8));

        final ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
        final UserData ud2 = Utils.OBJECT_MAPPER.readValue(bais, UserData.class);

        log.info("UserData:\n{}", ud2);
    }

    @Data
    public static class Item {

        @JsonFormat(pattern = Utils.DATETIME_PATTERN)
        private LocalDateTime localDateTime;

        private String hanso;

        public Item ldt(LocalDateTime localDateTime) {
            this.localDateTime = localDateTime;
            return this;
        }

        public boolean equals(Object other) {
            if (other == null || other.getClass() != this.getClass()) {
                return false;
            }
            return toString().equals(other.toString());
        }

        @Override
        public int hashCode() {
            return (localDateTime == null ? 13 : localDateTime.hashCode()) *
                    (hanso == null ? 7 : hanso.hashCode());
        }

        public String toString() {
            return getClass().getSimpleName() + "(localDateTime=" + (localDateTime == null ? null : localDateTime.format(Utils.SDF_JSON_DATETIME)) + ", hanso=" + hanso + ")";
        }
    }
}
