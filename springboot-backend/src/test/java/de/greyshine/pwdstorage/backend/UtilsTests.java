package de.greyshine.pwdstorage.backend;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.util.Assert;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

@Slf4j
public class UtilsTests {

    @Test
    public void encryptDecrypt() {

        final String password = "PaSsWoRd!";
        final String text = "Hello World!";

        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        Utils.encrypt(password, text, baos);

        log.info("\n{}", baos.toString(StandardCharsets.UTF_8));

        final ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
        final String decryptedText = Utils.decrypt(password, bais);

        log.info("Decrypted: {}", decryptedText);

        Assertions.assertEquals(text, decryptedText);
    }

    @Test
    public void encryptDecryptFile() throws IOException {

        final var file = new File("target/test-crypt.dat");
        if (file.exists()) {
            FileUtils.delete(file);
        }
        Assert.isTrue(!file.exists(), "File must not exist: " + file.getAbsolutePath());

        final var password = file.getName();
        final var text = "Hallo Welt!\nHello World!\nÂllo, le monde!";

        final Utils.EncodedOutputStream eos = new Utils.EncodedOutputStream(file, password);
        for (byte b : text.getBytes(StandardCharsets.UTF_8)) {
            eos.write(b);
        }
        eos.close();

        final Utils.EncodedInputStream eis = new Utils.EncodedInputStream(file, password);

        final String text2 = eis.readAll();

        eis.close();

        log.info("result: {}", text2);

        Assertions.assertEquals(text, text2);
    }


}