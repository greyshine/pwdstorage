package de.greyshine.pwdstorage.backend;

import de.greyshine.pwdstorage.backend.model.Entry;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.io.File;
import java.io.IOException;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, args = {"server", "home=./target/test-server-home"})
@AutoConfigureMockMvc
//@TestPropertySource(locations = "classpath:application-integrationtest.properties")
@Slf4j
class ServerRemoteTests {

    // also check start arguments at @SpringBootTest
    final static File HOME_SERVER = new File("./target/test-server-home");
    @Autowired
    private MockMvc mockMvc;

    @BeforeAll
    static void before() throws IOException {

        if (HOME_SERVER.exists() && HOME_SERVER.listFiles().length > 0) {
            FileUtils.forceDelete(HOME_SERVER);
        }

        HOME_SERVER.mkdirs();

        log.info("home={}", HOME_SERVER.getCanonicalPath());

        Assertions.assertTrue(HOME_SERVER.isDirectory(), "no homedir set: " + HOME_SERVER.getAbsolutePath());
        Assertions.assertTrue(HOME_SERVER.listFiles().length == 0, "HOME_SERVER expected to be empty.");
    }

    @Test
    void testLogin() throws Exception {

        final Utils.Wrapper<String> token = new Utils.Wrapper<>();

        mockMvc.perform(post("/server/register/user1")
                .contentType("plain/text")
                .content("User1!"))
                .andExpect(status().isOk())
                .andDo(rh -> token.set(rh.getResponse().getContentAsString()));

        log.info("token: {}", token.get());

        final Entry entry = new Entry();
        entry.setKey("Entry 1");
        entry.setValue("value1");

        String entryString = Utils.OBJECT_MAPPER.writeValueAsString(entry);

        log.info("request token: {}", token.get());
        log.info("entry string:\n{}", entryString);

        mockMvc.perform(post("/server/upsert")
                .header(Utils.WEB_HEADER_TOKEN, token.get())
                .content(entryString))
                .andExpect(status().isOk())
                .andDo(rh -> token.set(rh.getResponse().getContentAsString()));


    }
}
