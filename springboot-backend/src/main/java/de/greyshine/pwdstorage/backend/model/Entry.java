package de.greyshine.pwdstorage.backend.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDateTime;

import static de.greyshine.pwdstorage.backend.Utils.DATETIME_PATTERN;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Slf4j
public class Entry {

    private static int CREATION_IDS = 0;

    public final int c_id = CREATION_IDS++;

    public String id;
    //@JsonSerialize(using = LocalDateTimeSerializer.class)
    //@JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonFormat(pattern = DATETIME_PATTERN)
    public LocalDateTime created;
    //@JsonSerialize(using = LocalDateTimeSerializer.class)
    //@JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonFormat(pattern = DATETIME_PATTERN)
    public LocalDateTime updated;
    public String key;
    public String value;

    public static void trimEntry(Entry entry) {

        if (entry == null) {
            return;
        }

        entry.id = StringUtils.trimToNull(entry.id);
        entry.key = StringUtils.trimToNull(entry.key);
        entry.value = StringUtils.trimToNull(entry.value);
    }
}
