package de.greyshine.pwdstorage.backend.annotations;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;


@Aspect
@Component
@Slf4j
public class TokenAspect {

    /**
     * https://stackoverflow.com/a/62060417/845117
     *
     * @param joinPoint
     * @return
     * @throws Throwable
     */
    //@Around("execution(* de.greyshine.pwdstorage.backend.controller.*.*(..))")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {

        log.info("around {}", joinPoint);

        Object[] args = joinPoint.getArgs();

        for (Object arg : args) {
            //log.info("arg: {}", arg);
        }


        //System.out.println("Input :\n" + joinPoint.getArgs()[0]);

        // HttpServletRequest servletRequest = (HttpServletRequest) joinPoint.getArgs()[1];

        final Object result = joinPoint.proceed();

        //System.out.println(result);

        return result;
    }

}
