package de.greyshine.pwdstorage.backend.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static de.greyshine.pwdstorage.backend.Utils.DATETIME_PATTERN;

@Data
public class UserData {

    @JsonFormat(pattern = DATETIME_PATTERN)
    public LocalDateTime created;
    @JsonFormat(pattern = DATETIME_PATTERN)
    public LocalDateTime updated;

    public long version = 0;

    public final List<Entry> entries = new ArrayList<>();
    public String user;

    @JsonIgnore
    public UserData updateVersion() {
        version++;
        return this;
    }

    @JsonIgnore
    public Iterator<String> getEntryIds() {

        final var entriesIter = new ArrayList<>(this.entries).iterator();

        return new Iterator<>() {
            @Override
            public boolean hasNext() {
                return entriesIter.hasNext();
            }

            @Override
            public String next() {
                return entriesIter.next().getId();
            }
        };
    }

    @JsonIgnore
    public Entry getEntry(String entryId) {
        return entries.stream()
                .filter(entry -> StringUtils.equals(entryId, entry.getId()))
                .findFirst()
                .orElseThrow();
    }
}
