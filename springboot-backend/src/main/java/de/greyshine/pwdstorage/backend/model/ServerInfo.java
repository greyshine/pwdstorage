package de.greyshine.pwdstorage.backend.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.io.File;
import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.Map;

import static de.greyshine.pwdstorage.backend.Utils.DATETIME_PATTERN;

@Data
public class ServerInfo {

    private final Map<String, String> passwords = new LinkedHashMap<>();

    @JsonFormat(pattern = DATETIME_PATTERN)
    private LocalDateTime created;
    @JsonFormat(pattern = DATETIME_PATTERN)
    private LocalDateTime updated;

    private String rootPassword;
    @JsonIgnore
    private long lastUpdateTime;

    public void setFileTime(File file) {
        lastUpdateTime = file.lastModified();
    }
}
