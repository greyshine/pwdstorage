package de.greyshine.pwdstorage.backend.controller;

import de.greyshine.pwdstorage.backend.Utils;
import de.greyshine.pwdstorage.backend.annotations.SecuredRequest;
import de.greyshine.pwdstorage.backend.annotations.Token;
import de.greyshine.pwdstorage.backend.assertions.Assert;
import de.greyshine.pwdstorage.backend.model.Entry;
import de.greyshine.pwdstorage.backend.model.UserPassword;
import de.greyshine.pwdstorage.backend.service.DataService;
import de.greyshine.pwdstorage.backend.service.UserService;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@RestController
@Slf4j
public class ClientController {

    private final UserService userService;
    private final DataService dataService;

    public ClientController(UserService userService, DataService dataService) {
        this.userService = userService;
        this.dataService = dataService;
    }

    @GetMapping(value = "/about", produces = MediaType.TEXT_HTML_VALUE)
    public String about() throws URISyntaxException, IOException {

        final var aboutContent = Files.readString(Paths.get(getClass().getResource("/about.md").toURI()));
        return Utils.markdown2Html(aboutContent);
    }

    @PostMapping(value = "/api/register", produces = MediaType.TEXT_PLAIN_VALUE)
    public String register(@RequestBody UserPassword userPassword, HttpServletRequest request) {
        return userService.register(userPassword.getUser(), userPassword.getPassword(), Utils.getIp(request, true));
    }

    /**
     * @return a token if logged in
     */
    @PostMapping(value = "/api/login", produces = MediaType.TEXT_PLAIN_VALUE)
    public String login(@RequestBody UserPassword userPassword, HttpServletRequest request) throws IOException {
        final var ip = Utils.getIp(request, false);
        return userService.login(userPassword.getUser(), userPassword.getPassword(), ip);
    }

    //@PostMapping("/api/logout")
    public void logout(HttpServletRequest request) {
        // TODO token as parameter
        final String token = request.getHeader(Utils.WEB_HEADER_TOKEN);
        userService.logout(token);
    }

    @PostMapping("/api/logout")
    public void logout(@Token String token) {
        userService.logout(token);
    }

    @GetMapping(value = "/api/version", produces = MediaType.APPLICATION_JSON_VALUE)
    public String getVersionInfo() {
        return "TODO: get version from pom.xml";
    }

    @GetMapping("/api/data")
    @SecuredRequest
    public Data getData() throws IOException {

        final var user = userService.getCurrentUser();
        return new Data().addAll(dataService.getAll(user.getName(), user.getPassword()));
    }

    @PostMapping("/api/upsert")
    @SecuredRequest
    public Entry upsert(@RequestBody Entry entry) throws IOException {

        final var user = userService.getCurrentUser();
        return dataService.upsert(user.getName(), user.getPassword(), entry);
    }

    @DeleteMapping("/api/delete/{id}")
    @SecuredRequest
    public void delete(@PathVariable("id") String id) throws IOException {

        final var user = userService.getCurrentUser();
        final boolean isDeleted = dataService.delete(user.getName(), user.getPassword(), id);
        Assert.isTrue(isDeleted, "id", () -> "Delete failed. Id does not exist.");
    }

    @Getter
    public static class Data {

        private final String time = LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
        private final List<Entry> entries = new ArrayList<>();

        public Data addAll(Collection<Entry> datas) {
            entries.addAll(datas);
            return this;
        }
    }
}
