package de.greyshine.pwdstorage.backend.service;

import de.greyshine.pwdstorage.backend.Config;
import de.greyshine.pwdstorage.backend.Utils;
import de.greyshine.pwdstorage.backend.assertions.Assert;
import de.greyshine.pwdstorage.backend.model.Entry;
import de.greyshine.pwdstorage.backend.model.UserData;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import static de.greyshine.pwdstorage.backend.Utils.isBlank;
import static de.greyshine.pwdstorage.backend.assertions.Assert.*;

@Service
@Slf4j
public class DataService {

    private final Config config;

    public DataService(Config config) {
        this.config = config;
    }

    public List<Entry> getAll(String user, String password) throws IOException {

        Assert.isNotBlank(user, "user", () -> "user must not be blank");

        final UserData userData = loadUserData(user, password);
        return userData.getEntries();
    }

    public UserData loadUserData(String user, String password) throws IOException {

        Assert.isNotBlank(user, "user", null);
        Assert.isNotBlank(password, "password", null);

        final File file = getUserFile(user);

        Assert.isFile(file, () -> "No file for user=" + user);

        try (Utils.EncodedInputStream eis = new Utils.EncodedInputStream(file, password)) {

            final String json = eis.readAll();

            final UserData userData = Utils.OBJECT_MAPPER.readValue(json, UserData.class);

            log.info("loaded user file user={} with {} entries", user, userData.getEntries().size());

            return userData;
        }
    }

    public void saveInitial(String user, String password) {

        Assert.isNotBlank(user, "user", () -> "User is blank");
        Assert.isNotBlank(user, "password", () -> "Password is blank");

        final File file = getUserFile(user);
        Assert.isNoFile(file, "user-file", () -> "file for user does already exist");

        final LocalDateTime now = LocalDateTime.now();
        final UserData userData = new UserData();
        userData.setUser(user);
        userData.setCreated(now);
        userData.setUpdated(now);

        final Entry entry1 = new Entry();
        entry1.setId("1");
        entry1.setCreated(now);
        entry1.setUpdated(now);
        entry1.setKey("Welcome!");
        entry1.setValue("We welcome you!\nLet us introduce you to this tool...\nOf course you may delete this entry.");
        userData.getEntries().add(entry1);

        if (config.isCreateTestData() && "test".equalsIgnoreCase(user)) {
            log.warn("creating test user data!");
            userData.getEntries().addAll(config.getTestData());
        }

        try (Utils.EncodedOutputStream eos = new Utils.EncodedOutputStream(file, password)) {

            Utils.OBJECT_MAPPER.writeValue(eos, userData);

        } catch (IOException exception) {
            exception.printStackTrace();
        }

        log.info("created user: {} at {}", user, file.getAbsolutePath());
    }

    public void save(String user, String password, String entryId) throws IOException {

        Assert.isNotBlank(user, "user", null);
        Assert.isNotBlank(password, "password", () -> "No storage password for user=" + user);


        final UserData userData = loadUserData(user, password);
        Assert.isNotNull(userData, "UserData", () -> "no userdata for user=" + user);

        doVersionCheck(user, password, entryId, userData);

        userData.version++;
        userData.setUpdated(LocalDateTime.now());

        final var file = getUserFile(user);

        try (Utils.EncodedOutputStream eos = new Utils.EncodedOutputStream(file, password)) {
            Utils.OBJECT_MAPPER.writeValue(eos, userData);
        }
    }

    private void doVersionCheck(String user, String password, String entryId, UserData userData) throws IOException {

        final var savedUserData = loadUserData(user, password);

        if (userData.version == savedUserData.version) {
            return;
        }

        var savedEntry = savedUserData.getEntry(entryId);
        var entryToCheck = userData.getEntry(entryId);

        if (StringUtils.equals(savedEntry.getKey(), entryToCheck.getKey())
                && StringUtils.equals(savedEntry.getValue(), entryToCheck.getValue())) {
            return;
        }

        final String newValue = entryToCheck.getValue() + "\n\n" +
                "<Version mismatch>\n" +
                savedEntry.getKey() + "\n\n" +
                savedEntry.getValue();

        entryToCheck.setValue(newValue);
    }

    public Entry getById(String user, String password, String id) throws IOException {

        isNotBlank(id, "id", () -> "id must not be blank");

        final var userData = loadUserData(user, password);
        return userData.getEntry(id);
    }

    public Entry getByKey(String user, String password, String key) throws IOException {

        isNotBlank(key, "key", () -> "key must not be blank");

        final List<Entry> entries = loadUserData(user, password).getEntries();

        final var resultWrapper = new Utils.Wrapper<Entry>();
        entries.forEach(e -> {

            if (!e.getKey().equals(key)) {
                return;
            }

            if (!resultWrapper.isNull()) {
                log.error("There is more than one entry with same key first: {}\nother: {}", resultWrapper.get(), e);
            } else {
                resultWrapper.set(e);
            }
        });

        return resultWrapper.get();
    }

    public boolean delete(String user, String password, String entryId) throws IOException {

        final List<Entry> entries = loadUserData(user, password).getEntries();

        for (Entry entry : entries) {
            if (entryId.equals(entry.getId())) {
                entries.remove(entry);
                save(user, password, entryId);
                return true;
            }
        }

        return false;
    }

    public Entry upsert(String user, String password, Entry entry) throws IOException {

        isNotNull(entry, "entry", () -> "entry must not be null");

        return isBlank(entry.getId()) ? create(user, password, entry) : update(user, password, entry);
    }

    public Entry update(String user, String password, Entry entry) throws IOException {

        isNotNull(entry, null, () -> "entry must not be null");

        Entry.trimEntry(entry);

        isNotNull(entry.key, "key", () -> "key is not set on entry '" + entry.getId() + "'");

        // Check on key
        final Entry entryByKey = getByKey(user, password, entry.key);

        if (entryByKey != null && !Utils.isEquals(entry.id, entryByKey.id)) {
            fail("key", () -> "key is taken by other (id=" + entryByKey.getId() + ")");
        }

        final Entry persistedEntry = getById(user, password, entry.getId());
        isNotNull(persistedEntry, "key", () -> "entry does not exist with id=" + entry.id);

        persistedEntry.key = entry.key;
        persistedEntry.value = entry.value;

        log.info(">>> update CID={} value={}", entry.c_id, entry.value);

        persistedEntry.updated = LocalDateTime.now();

        save(user, password, persistedEntry.getId());

        return persistedEntry;
    }

    private Entry create(String user, String password, Entry entry) throws IOException {

        isNotNull(entry, null, () -> "entry must not be null");

        Entry.trimEntry(entry);

        isNull(entry.id, "id", () -> "id must be unset");
        isNotBlank(entry.key, "key", () -> "key must be set");
        isNull(entry.created, "created", () -> "created must be blank");
        isNull(entry.updated, "updated", () -> "updated must be blank");

        entry.id = UUID.randomUUID().toString();

        entry.created = LocalDateTime.now();
        entry.updated = entry.created;

        final UserData userData = loadUserData(user, password);
        userData.getEntries().add(entry);

        save(user, password, entry.getId());

        return entry;
    }


    protected File getUserFile(String user) {

        Assert.isNotBlank(user, "user", () -> "user must not be blank");

        final File file = new File(config.getHomeDir(), user.toLowerCase(Locale.ROOT) + ".json");

        log.info("getUserFile({})={}, exists={}", user, file.getAbsolutePath(), file.exists());

        return file;
    }
}
