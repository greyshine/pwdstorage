package de.greyshine.pwdstorage.backend.assertions;

import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.util.function.Supplier;

@Slf4j
public class Assert {

    public static void fail(String field, Supplier<String> message) {
        message = message != null ? message : () -> "failure on field " + field;
        throw new AssertException(field, null, message.get());
    }

    public static void isNull(Object value, String field, Supplier<String> message) {

        if (value == null) {
            return;
        }

        message = message != null ? message : () -> field + " is set";

        throw new AssertException(field, value, message.get());
    }

    public static void isNotNull(Object value, String field, Supplier<String> message) {

        if (value != null) {
            return;
        }

        message = message != null ? message : () -> field + " must not be null";

        log.error("assert failure value is null on field '{}': {}", field, message.get());

        throw new AssertException(field, value, message.get());
    }

    public static void isBlank(String value, String field, Supplier<String> message) {

        if (value == null || value.trim().isEmpty()) {
            return;
        }

        message = message != null ? message : () -> field + " must be blank, but is '" + value + "'";


    }

    public static void isNotBlank(String value, String field, Supplier<String> message) {

        if (value != null && !value.trim().isEmpty()) {
            return;
        }

        message = message != null ? message : () -> field + " must not be blank, but is " +
                value == null ? "<null>" : "'" + value + "'";

        throw new AssertException(field, value, message.get());
    }

    public static void isTrue(boolean value, String field, Supplier<String> message) {

        if (value) {
            return;
        }

        message = message != null ? message : () -> field + " must be true";

        throw new AssertException(field, value, message.get());
    }

    public static void isFalse(boolean value, String field, Supplier<String> message) {

        if (!value) {
            return;
        }

        message = message != null ? message : () -> field + " must be false";

        throw new AssertException(field, value, message.get());
    }

    /**
     * @param checkValue  the value to check on equality against the assert value
     * @param assertValue the value expected to be
     * @param field       name of the concerned field
     * @param message
     */
    public static void equalsNotNull(Object checkValue, Object assertValue, String field, Supplier<String> message) {

        if (checkValue != null && checkValue.equals(assertValue)) {
            return;
        }

        message = message != null ? message : () -> "On " + field + " value " + checkValue == null ? null : ("'" + checkValue + "'") + " is not equals " + assertValue;

        throw new AssertException(field, checkValue, message.get());
    }

    public static void isNoFile(File file, String field, Supplier<String> message) {

        isNotNull(file, "file", () -> "File must not be null to be checked on noFile-Check");

        if (!file.exists()) {
            return;
        }

        message = message != null ? message : () -> "File '" + file.getAbsolutePath() + "' must not exist.";

        throw new AssertException(field, file, message.get());
    }

    public static void isLocalIp(String ip) {

        if ("127.0.0.1".equals(ip) || "0:0:0:0:0:0:0:1".equals(ip)) {
            return;
        }

        throw new AssertException("ip", ip, "The given ip=" + ip + " is not local 127.0.0.1");
    }

    public static void isFile(File file, Supplier<String> message) {

        if (file != null && file.isFile() && file.canRead()) {
            return;
        }

        message = message != null ? message : () -> "File " + (file == null ? "<null>" : "'" + file.getAbsolutePath() + "'") +
                " does not exist.";

        throw new AssertException("File", file, message.get());

    }

    public static void isTrimmed(String value, boolean allowNull, String field, Supplier<String> message) {

        if (value == null && allowNull) {
            return;
        } else if (value != null && value.trim().length() == value.length()) {
            return;
        }

        message = message != null ? message : () -> "value " + value + " is not trimmed";

        throw new AssertException(field, value, message.get());
    }

    public static void isNotEquals(Object o1, Object o2, String field, Supplier<String> message) {

        if (o1 == o2) {
            return;
        }

        final boolean isEquals = o1 == null || !o1.equals(o2);

        if (isEquals) {
            return;
        }

        final var value = "[" + o1 + ", " + o2 + "]";
        message = message != null ? message : () -> "Objects are not equal [" + o1 + ", " + o2 + "]";

        throw new AssertException(field, value, message.get());
    }
}
