package de.greyshine.pwdstorage.backend.assertions;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper = false)
public class AssertException extends RuntimeException {

    public final LocalDateTime time = LocalDateTime.now();
    public final String field;
    public final Object value;
    public final String message;

}
