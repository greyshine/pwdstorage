package de.greyshine.pwdstorage.backend.controller;

import de.greyshine.pwdstorage.backend.assertions.AssertException;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@ControllerAdvice
@Slf4j
public class WebExceptionHandler {

    @ExceptionHandler(AssertException.class)
    @ResponseBody
    public ResponseEntity<ErrorMsg> handle(AssertException exception, HttpServletRequest request, HttpServletResponse response) {

        log.error("{}: {}", exception.getClass().getCanonicalName(), exception.getMessage());

        exception.printStackTrace();

        var errorMsg = new ErrorMsg()
                .exception(exception)
                .field(exception.getField())
                .message(exception.getMessage());

        return new ResponseEntity<>(errorMsg, HttpStatus.PRECONDITION_FAILED);
    }

    @ExceptionHandler(IllegalAccessException.class)
    @ResponseBody
    public ResponseEntity<ErrorMsg> handle(IllegalAccessException exception, HttpServletRequest request, HttpServletResponse response) {

        log.info("{}: {}", exception.getClass().getCanonicalName(), exception.getMessage());

        return new ResponseEntity<>(new ErrorMsg(exception), HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(IOException.class)
    @ResponseBody
    public ResponseEntity<ErrorMsg> handle(IOException exception, HttpServletRequest request, HttpServletResponse response) {

        log.error("{}: {}", exception.getClass().getCanonicalName(), exception.getMessage());

        var errorMsg = new ErrorMsg(exception);
        return new ResponseEntity<>(errorMsg, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(ResponseStatusException.class)
    @ResponseBody
    public ResponseEntity<ErrorMsg> handle(ResponseStatusException exception, HttpServletRequest request, HttpServletResponse response) {

        var errorMsg = new ErrorMsg(exception);

        if (exception.getStatus() == HttpStatus.NOT_FOUND) {
            errorMsg.exception(null)
                    .message(HttpStatus.NOT_FOUND.name());
        }

        return new ResponseEntity<>(errorMsg, exception.getStatus());
    }

    @ExceptionHandler({Exception.class, RuntimeException.class, Error.class})
    @ResponseBody
    public ResponseEntity<ErrorMsg> handle(Exception exception, HttpServletRequest request, HttpServletResponse response) {

        var errorMsg = new ErrorMsg(exception);

        log.error("{}: {}", exception.getClass().getCanonicalName(), exception.getMessage(), exception);

        return new ResponseEntity<>(errorMsg, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @Data
    public static final class ErrorMsg {

        public final String date = LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
        public String exception;
        public String message;
        public String field;

        public ErrorMsg() {
        }

        public ErrorMsg(Exception e) {
            exception(e);
        }

        public ErrorMsg exception(Exception exception) {
            this.exception = exception == null ? null : exception.getMessage();
            return this;
        }

        public ErrorMsg message(String message) {
            this.message = message;
            return this;
        }

        public ErrorMsg field(String field) {
            this.field = field;
            return this;
        }
    }

}
