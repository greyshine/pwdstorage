package de.greyshine.pwdstorage.backend.model;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.UUID;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * The entity just lives in the cache and is not persisted on a permanent storage.
 */
@Data
@Slf4j
public class User {

    private final String name;
    private final byte[] password;
    private final String token = UUID.randomUUID().toString();
    private final String ip;
    private LocalDateTime lastAccess;

    public User(String name, String password, String ip) {
        this.name = name;
        this.password = password == null ? null : password.getBytes(StandardCharsets.UTF_8);
        this.ip = ip;
    }

    public String getPassword() {
        return this.password == null ? null : new String(this.password);
    }

    public boolean isAlive(int maxTimeToLiveMinutes) {
        final LocalDateTime maxTimeToLive = this.lastAccess.plus(maxTimeToLiveMinutes, ChronoUnit.MINUTES);
        return !LocalDateTime.now().isAfter(maxTimeToLive);
    }

    public boolean updateLastAccess(int maxTimeToLiveMinutes) {

        if (!isAlive(maxTimeToLiveMinutes)) {
            return false;
        }

        this.lastAccess = LocalDateTime.now();
        return true;
    }

    public boolean isToken(String token) {
        return StringUtils.equals(this.token, token);
    }

    public boolean isToken(String token, String ip) {
        return isToken(token) && isNotBlank(ip) && StringUtils.equals(this.ip, ip);
    }
}
