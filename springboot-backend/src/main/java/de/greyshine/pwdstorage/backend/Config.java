package de.greyshine.pwdstorage.backend;

import de.greyshine.pwdstorage.backend.model.Entry;
import de.greyshine.pwdstorage.backend.model.ServerInfo;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.boot.ApplicationArguments;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.Assert;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

@Configuration
@Data
@Slf4j
public class Config {

    static final String rootpasswordFilename = "rootpassword.txt";

    private final File homeDir;
    private final boolean isTestData;

    private ServerInfo serverInfo;

    public Config(ApplicationArguments aArgs) throws IOException {

        log.info("args: {}", aArgs.getSourceArgs());

        File homeDir = new File(FileUtils.getUserDirectory().getCanonicalFile(), ".pwdstorage").getAbsoluteFile();
        boolean isTestData = false;
        boolean isServer = false;

        for (String arg : aArgs.getNonOptionArgs()) {

            if (arg.toLowerCase(Locale.ROOT).startsWith("home=")) {

                homeDir = evaluateHomeDir(arg);

            } else {

                switch (arg) {
                    case "server":
                    case "server=true":
                        isServer = true;
                        continue;

                    case "test":
                        isTestData = true;
                        log.info("creating test data for user 'test'");
                        continue;
                }
            }
        }

        Assert.notNull(homeDir, "no home directory set as argument");

        log.info("homeDir: {}", homeDir);

        this.homeDir = homeDir;
        this.isTestData = isTestData;

        if (isServer) {
            log.info("Running as server.");
            this.serverInfo = initServer();
        } else {
            log.info("Running as standalone/client.");
        }
    }

    /**
     * When the server starts
     * <p>
     * it checks:
     * <p>
     * - if the server.json exists
     * - if no such file exists it expects to have set the root and the server password in separate files
     */
    private ServerInfo initServer() throws IOException {

        ServerInfo serverInfo = null;

        final File serverJsonFile = new File(homeDir, "server.json");
        log.info("server file: {} (exists={})", serverJsonFile, serverJsonFile.isFile());

        if (!serverJsonFile.exists()) {

            serverInfo = new ServerInfo();
            serverInfo.setCreated(LocalDateTime.now());
            serverInfo.setUpdated(serverInfo.getCreated());

        } else {

            serverInfo = Utils.OBJECT_MAPPER.readValue(serverJsonFile, ServerInfo.class);
            serverInfo.setFileTime(serverJsonFile);
        }

        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        Utils.OBJECT_MAPPER.writeValue(baos, serverInfo);
        log.info("File:\n{}", baos.toString(StandardCharsets.UTF_8));

        Utils.OBJECT_MAPPER.writeValue(serverJsonFile, serverInfo);
        serverInfo.setFileTime(serverJsonFile);

        // TODO: start concurrent thread checkin on the file time in order to recognize chnages directly on the physical
        // file

        return serverInfo;
    }

    private File evaluateHomeDir(String homeArg) throws IOException {

        final File localDir = new File(".").getCanonicalFile();
        final File homeDir = FileUtils.getUserDirectory().getCanonicalFile();
        File appDir = new File(homeDir, ".pwdstorage").getAbsoluteFile();

        log.info("localDir: {}", localDir);
        log.info("homeDir: {}", homeDir);
        log.info("appDir (exists={}): {}", appDir.exists(), appDir);

        File appDirOverwrite;
        homeArg = homeArg.substring(5);

        if (homeArg.startsWith("./")) {
            appDirOverwrite = new File(localDir, homeArg).getCanonicalFile();
        } else if (homeArg.startsWith("/")) {
            appDirOverwrite = new File(homeArg).getCanonicalFile();
        } else {
            appDirOverwrite = new File(homeDir, homeArg).getCanonicalFile();
        }

        appDir = appDirOverwrite;
        log.info("appDir overwritten (exists={}): {} ", appDir.exists(), appDir);

        if (!appDir.exists()) {
            FileUtils.forceMkdir(appDir);

            if (appDir.isDirectory()) {
                log.info("created appDir: {} ", appDir);
            }
        }

        Assert.isTrue(!appDir.exists() || appDir.isDirectory(), "cannot access appDir=" + appDir);


        FileUtils.forceMkdir(appDir);
        return appDir;
    }

    /**
     * @deprecated to be moved to own project
     */
    @Deprecated
    public boolean isServer_() {
        return serverInfo != null;
    }

    public boolean isCreateTestData() {
        return this.isTestData;
    }

    public List<Entry> getTestData() {

        var testwords = new String[]{"Auto", "Haus", "Musik", "Fliege", "Mücke", "Elefant", "Kaffee", "Keller"};

        var ids = new String[]{"ID-1", "ID-2", "ID-3", "ID-4"};

        final var list = new ArrayList<Entry>();

        for (int i = 1; i <= 45; i++) {

            var id = UUID.randomUUID().toString();

            if (i - 1 < ids.length) {
                //log.info("overwritten {}: {} > {}", i, id, ids[i-1] );
                id = ids[i - 1];
            }

            list.add(new Entry(id,
                    LocalDateTime.now(),
                    LocalDateTime.now(),
                    String.valueOf(id),
                    "Some value\nSome other Line\neven more line and long text since it just can be very long.\n\n" + testwords[i % testwords.length] + "\n\n" + i + "::" + id
            ));
        }

        return list;
    }
}
