package de.greyshine.pwdstorage.backend.service;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class BadLoginException extends RuntimeException {
    public final String user;
}
