package de.greyshine.pwdstorage.backend;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.node.TextNode;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import de.greyshine.pwdstorage.backend.assertions.Assert;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.apache.commons.io.IOUtils;
import org.commonmark.parser.Parser;
import org.commonmark.renderer.html.HtmlRenderer;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;
import java.util.Locale;
import java.util.function.Consumer;
import java.util.function.Function;

import static java.time.format.DateTimeFormatter.ISO_OFFSET_DATE_TIME;

public abstract class Utils {

    public static final String WEB_HEADER_TOKEN = "TKN";

    public static final String DATETIME_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS";
    public static final DateTimeFormatter SDF_JSON_DATETIME = DateTimeFormatter.ofPattern(DATETIME_PATTERN);

    public static final ObjectMapper OBJECT_MAPPER = new ObjectMapper()
            .registerModule(new JavaTimeModule())
            .setSerializationInclusion(JsonInclude.Include.ALWAYS)
            .enable(SerializationFeature.INDENT_OUTPUT);


    private static final String[] IP_HEADER_CANDIDATES = {
            "X-Forwarded-For",
            "Proxy-Client-IP",
            "WL-Proxy-Client-IP",
            "HTTP_X_FORWARDED_FOR",
            "HTTP_X_FORWARDED",
            "HTTP_X_CLUSTER_CLIENT_IP",
            "HTTP_CLIENT_IP",
            "HTTP_FORWARDED_FOR",
            "HTTP_FORWARDED",
            "HTTP_VIA",
            "REMOTE_ADDR"};

    private Utils() {
    }

    /**
     * null is never equals
     *
     * @return whether it's equal or not ;-)
     */
    public static boolean isEquals(Object o1, Object o2) {
        return isEquals(o1, o2, false);
    }

    public static boolean isEquals(Object o1, Object o2, boolean isNullEquals) {

        if (o1 == o2) {
            return o1 != null || isNullEquals;
        } else if (o1 == null ^ o2 == null) {
            return false;
        } else {
            return o1.equals(o2);
        }
    }

    public static boolean isBlank(String s) {
        return s == null || s.trim().isEmpty();
    }

    public static boolean isNotBlank(String s) {
        return !isBlank(s);
    }

    public static String trimToNull(String s) {
        return isBlank(s) ? null : s.trim();
    }

    public static String trimToEmpty(String s) {
        return isBlank(s) ? "" : s.trim();
    }

    public static boolean ENCODE = true;

    public static boolean isProperPassword(String password) {

        if (isBlank(password)) {
            return false;
        } else if (password.trim().length() != password.length()) {
            return false;
        } else if (!password.replaceAll("\\s", "").equals(password)) {
            return false;
        } else if (password.toLowerCase(Locale.ROOT).equals(password)) {
            return false;
        } else if (password.toUpperCase(Locale.ROOT).equals(password)) {
            return false;
        } else if (!password.matches(".*[0-9].*")) {
            return false;
        } else if (!password.matches(".*[!\"§$%&/\\\\()\\[\\]{}=?*+'#;,:.\\-_<>°^|].*")) {
            return false;
        } else {
            return password.length() >= 6;
        }
    }

    @SneakyThrows
    public static byte[] toSha256Hash(String s) {

        if (s == null) {
            return null;
        }

        final MessageDigest digest = MessageDigest.getInstance("SHA-256");
        return digest.digest(s.getBytes(StandardCharsets.UTF_8));
    }

    /**
     * https://stackoverflow.com/a/11009612/845117
     *
     * @param s String to be hashed
     * @return the hash 256
     */
    @SneakyThrows
    public static String toSha256(String s) {

        if (s == null) {
            return null;
        }

        final byte[] hash = toSha256Hash(s);

        final StringBuilder hexString = new StringBuilder();
        for (int i = 0; i < hash.length; i++) {
            final String hex = Integer.toHexString(0xff & hash[i]);
            if (hex.length() == 1) {
                hexString.append('0');
            }
            hexString.append(hex);
        }

        return hexString.toString();
    }

    @SneakyThrows
    public static void encrypt(String password, String text, OutputStream encodedOutputStream) {

        final byte[] hash = toSha256Hash(password);
        System.out.println("hash-len: " + hash.length);
        final byte[] textBytes = text.getBytes(StandardCharsets.UTF_8);

        int hashIndex = 0;

        for (byte b : textBytes) {

            byte encodedByte = (byte) (b ^ hash[hashIndex++] ^ (hash.length - hashIndex));
            hashIndex = hashIndex == hash.length ? 0 : hashIndex;

            encodedOutputStream.write(encodedByte);
        }

        encodedOutputStream.flush();
    }

    @SneakyThrows
    public static String decrypt(String password, InputStream encodedInputStream) {

        final byte[] hash = toSha256Hash(password);
        final StringBuilder sb = new StringBuilder();

        int hashIndex = 0;
        for (byte b : encodedInputStream.readAllBytes()) {

            byte decodedByte = (byte) (b ^ hash[hashIndex++] ^ (hash.length - hashIndex));
            hashIndex = hashIndex == hash.length ? 0 : hashIndex;

            sb.append((char) decodedByte);
        }

        return sb.toString();
    }

    public static String markdown2Html(String markdown) {

        if (markdown == null) {
            return null;
        }

        // :-) https://simplesolution.dev/java-spring-boot-convert-markdown-to-html-using-commonmark/
        return HtmlRenderer.builder().build().render(Parser.builder().build().parse(markdown));
    }

    @AllArgsConstructor
    @NoArgsConstructor
    public static class Wrapper<T> {

        public T object;

        public T get() {
            return object;
        }

        public void set(T object) {
            this.object = object;
        }

        public boolean isNull() {
            return object == null;
        }

        public void consume(Consumer<T> consumer) {
            org.springframework.util.Assert.notNull(consumer, "parameter consumer is null");
            consumer.accept(object);
        }

        @Override
        public String toString() {
            return object + " (Wrapper)";
        }
    }

    public static class LocalDateTimeJsonSerializer extends JsonSerializer<LocalDateTime> {
        @Override
        public void serialize(LocalDateTime localDateTime, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
            final String dateString = localDateTime == null ? null : localDateTime.format(ISO_OFFSET_DATE_TIME);
            jsonGenerator.writeString(dateString);
        }
    }

    public static class LocalDateTimeJsonDeserializer extends JsonDeserializer<LocalDateTime> {
        @Override
        public LocalDateTime deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
            final ObjectCodec oc = jsonParser.getCodec();
            final TextNode node = oc.readTree(jsonParser);
            final String dateString = node.textValue();
            return dateString == null ? null : LocalDateTime.parse(dateString, ISO_OFFSET_DATE_TIME);
        }
    }

    @SneakyThrows
    public static Iterator<Byte> getHashSha256Iterator(String s) {

        Assert.isNotBlank(s, "string", () -> "string to be hashed must not be blank");

        final MessageDigest digest = MessageDigest.getInstance("SHA-256");

        final byte[] bytes = digest.digest(s.getBytes(StandardCharsets.UTF_8));

        return new Iterator<>() {

            int i = 0;

            @Override
            public boolean hasNext() {
                return true;
            }

            @Override
            public Byte next() {

                final byte b = bytes[i];

                i = i == bytes.length - 1 ? 0 : i + 1;

                return b;
            }
        };
    }

    public static String getIp(HttpServletRequest request, boolean isSimple) {

        final Function<String, String> normalizer = (ip) ->
                "0:0:0:0:0:0:0:1".equals(ip) ? "127.0.0.1" : ip;

        if (!isSimple) {
            for (String header : IP_HEADER_CANDIDATES) {
                String ip = request.getHeader(header);
                if (ip != null && ip.length() != 0 && !"unknown".equalsIgnoreCase(ip)) {
                    return normalizer.apply(ip);
                }
            }
        }

        return normalizer.apply(request.getRemoteAddr());
    }

    public static File ensureExistingFile(File file) throws IOException {

        if (!file.isFile()) {
            file.createNewFile();
        }

        return file;
    }

    public static class EncodedOutputStream extends OutputStream {

        private final OutputStream out;
        private final Iterator<Byte> iter;

        public EncodedOutputStream(File file, String password) throws IOException {
            this(new FileOutputStream(ensureExistingFile(file)), password);
        }

        public EncodedOutputStream(OutputStream out, String password) {
            this.out = out;
            this.iter = getHashSha256Iterator(password);
        }

        @Override
        public void write(int b) throws IOException {

            if (!ENCODE) {
                this.out.write(b);
                return;
            }

            this.out.write(iter.next() ^ b);
        }

        @Override
        public void flush() throws IOException {
            this.out.flush();
        }

        @Override
        public void close() throws IOException {
            this.flush();
            this.out.close();
        }
    }

    public static class EncodedInputStream extends InputStream {

        private final InputStream inputStream;
        private final Iterator<Byte> iter;

        public EncodedInputStream(File file, String password) throws IOException {
            this(new FileInputStream(file), password);
        }

        public EncodedInputStream(InputStream inputStream, String password) {
            this.inputStream = inputStream;
            this.iter = getHashSha256Iterator(password);
        }

        @Override
        public int read() throws IOException {

            final int r = inputStream.read();

            if (r == -1) {
                return -1;
            }

            if (!ENCODE) {
                return r;
            }

            return r ^ iter.next();
        }

        @Override
        public void close() throws IOException {
            this.inputStream.close();
        }

        public String readAll() throws IOException {
            return IOUtils.toString(this, StandardCharsets.UTF_8);
        }
    }
}
