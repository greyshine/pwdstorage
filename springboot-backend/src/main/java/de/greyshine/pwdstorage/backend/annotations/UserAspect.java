package de.greyshine.pwdstorage.backend.annotations;


import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

// https://fullstackdeveloper.guru/2021/06/15/how-to-create-a-custom-annotation-in-spring-boot/
@Aspect
@Component
@Slf4j
public class UserAspect {

    public UserAspect() {
        log.info("{}", this);
    }

    /**
     * https://stackoverflow.com/a/62060417/845117
     *
     * @param joinPoint
     * @return
     * @throws Throwable
     */
    //@Around("execution(* de.greyshine.pwdstorage.backend.controller.*.*(..))")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {

        Object[] args = joinPoint.getArgs();

        for (Object arg : args) {
            log.info("arg: {}", arg);
        }


        System.out.println("Input :\n" + joinPoint.getArgs()[0]);

        HttpServletRequest servletRequest = (HttpServletRequest) joinPoint.getArgs()[1];

        Object result = joinPoint.proceed();

        System.out.println(result);

        return result;
    }
}
