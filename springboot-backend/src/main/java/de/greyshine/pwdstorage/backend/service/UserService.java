package de.greyshine.pwdstorage.backend.service;

import de.greyshine.pwdstorage.backend.Utils;
import de.greyshine.pwdstorage.backend.assertions.Assert;
import de.greyshine.pwdstorage.backend.model.User;
import de.greyshine.pwdstorage.backend.model.UserData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
public class UserService {

    /**
     * Der UserService hält immer alle Daten die zur Laufzeit gecached werden.
     * Niemals der DataService
     */
    private static final ThreadLocal<User> TL_CURRENTUSER = new ThreadLocal<>();
    // <Token, User>
    private static final Map<String, User> LOGGEDIN_USERS = new HashMap<>();

    private static final int maxTimeToLiveMinutes = 1;

    private final DataService dataService;

    public UserService(DataService dataService) {
        this.dataService = dataService;
    }

    public String login(String userName, String password, String ip) throws IOException {

        // TODO check if user is already logged in and token is still valid
        if (LOGGEDIN_USERS.containsKey(userName) && LOGGEDIN_USERS.get(userName).isAlive(maxTimeToLiveMinutes)) {
            log.warn("user is already logged in: {}", userName);
            throw new BadLoginException(userName);
        }

        final UserData userData = dataService.loadUserData(userName, password);
        final User user = new User(userName, password, ip);

        LOGGEDIN_USERS.put(user.getToken(), user);
        TL_CURRENTUSER.set(user);

        return user.getToken();
    }

    public void logout(String token) {
        LOGGEDIN_USERS.remove(token);
        TL_CURRENTUSER.remove();
    }

    /**
     * @param userName
     * @param password
     * @return the token from the successful registration (and login)
     */
    public String register(String userName, String password, String ip) {

        userName = Utils.trimToNull(userName);
        password = Utils.trimToNull(password);

        final boolean isTestUser = "test".equalsIgnoreCase(userName);

        Assert.isNotBlank(userName, "user", null);
        Assert.isNotBlank(ip, "ip", () -> "Ip must be set");
        Assert.isNotEquals(userName, "server", "user", () -> "user must not be named server");
        Assert.isNotBlank(password, "password", null);
        Assert.isTrue(!isTestUser || "test".equals(password), "password",
                () -> "password for test user must be 'test'");
        Assert.isTrue(Utils.isProperPassword(password) || isTestUser, "password",
                () -> "Password  must be 6 characters, at least 1 upper and lowercase and at least 1 special " +
                        "character");
        Assert.isTrue(!isTestUser || "test".equals(password), "password",
                () -> "Password must be test with user test");
        Assert.isNoFile(dataService.getUserFile(userName), "user", () -> "User already exists");

        dataService.saveInitial(userName, password);

        final var user = new User(userName, password, ip);

        TL_CURRENTUSER.set(user);
        LOGGEDIN_USERS.put(user.getToken(), user);

        log.info("registered {} (ip={}}); token={}", user, user.getToken());

        return user.getToken();
    }

    public boolean isToken(String token, String ip) {
        final var user = LOGGEDIN_USERS.get(token);
        return user != null && user.isToken(token, ip);
    }


    public User getCurrentUser() {
        return TL_CURRENTUSER.get();
    }

    public boolean setCurrentUserByToken(String token, String ip) {

        final var user = LOGGEDIN_USERS.get(token);

        if (user != null && user.isToken(token, ip)) {
            TL_CURRENTUSER.set(user);
            return true;
        }

        return false;
    }
}
