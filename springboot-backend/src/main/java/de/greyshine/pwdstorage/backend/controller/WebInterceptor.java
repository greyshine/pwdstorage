package de.greyshine.pwdstorage.backend.controller;

import de.greyshine.pwdstorage.backend.Config;
import de.greyshine.pwdstorage.backend.Utils;
import de.greyshine.pwdstorage.backend.annotations.SecuredRequest;
import de.greyshine.pwdstorage.backend.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
@Slf4j
public class WebInterceptor implements HandlerInterceptor {

    private final Config config;
    private final UserService userService;

    private WebInterceptor(Config config, UserService userService) {
        this.config = config;
        this.userService = userService;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        log.info("preHandle: {}, {}", request.getRequestURI(), handler.getClass().getCanonicalName());

        if (HandlerMethod.class != handler.getClass()) {
            return HandlerInterceptor.super.preHandle(request, response, handler);
        }

        final var requestUri = request.getRequestURI();

        if ("/about".equalsIgnoreCase(requestUri)) {
            return HandlerInterceptor.super.preHandle(request, response, handler);
        } else if (!requestUri.startsWith("/api/")) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        final HandlerMethod handlerMethod = (HandlerMethod) handler;

        final var isSecured = handlerMethod.getMethodAnnotation(SecuredRequest.class) != null;
        final var token = request.getHeader(Utils.WEB_HEADER_TOKEN);

        log.info("secured {} > {}, TKN={}", handlerMethod.getMethod().getDeclaringClass().getCanonicalName() + "." + handlerMethod.getMethod().getName(), isSecured, token);

        if (!isSecured) {
            return HandlerInterceptor.super.preHandle(request, response, handler);
        } else if (!userService.isToken(token, Utils.getIp(request, false))) {
            throw new IllegalAccessException("Not logged-in. Disallowed to access " + handlerMethod.getMethod());
        }

        userService.setCurrentUserByToken(token, Utils.getIp(request, false));

        return HandlerInterceptor.super.preHandle(request, response, handler);
    }
}
