package de.greyshine.pwdstorage.backend;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Slf4j
public class SprinbootMain {

    private final Config configuration;

    public SprinbootMain(Config configuration) {
        this.configuration = configuration;
    }

    /**
     * The home directory defaults to home=&lt;USERPATH&gt;/.pwdstorage
     * It may be specified differently.
     * <p>
     * args:
     * - home=/fullpath or home=./localpath home=&lt;USERPATH&gt;/declaredpath
     *
     */
    public static void main(String[] args) {
        SpringApplication.run(SprinbootMain.class, args);
    }
}
