package de.greyshine.pwdstorage.backend.controller;

import de.greyshine.pwdstorage.backend.Utils;
import de.greyshine.pwdstorage.backend.annotations.SecuredRequest;
import de.greyshine.pwdstorage.backend.annotations.User;
import de.greyshine.pwdstorage.backend.model.Entry;
import de.greyshine.pwdstorage.backend.service.DataService;
import de.greyshine.pwdstorage.backend.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;
import javax.websocket.server.PathParam;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

/**
 * Login User
 * Register User
 * CRUD User-Data
 * Delete User
 */
@Slf4j
public class ServerController {

    private final UserService userService;
    private final DataService dataService;

    public ServerController(UserService userService, DataService dataService) {
        this.userService = userService;
        this.dataService = dataService;
    }

    @PostMapping("/server/register/{user}")
    public ResponseEntity<String> register(@PathVariable("user") String user, @RequestBody String password, HttpServletRequest httpServletRequest) throws IOException {

        final String ip = Utils.getIp(httpServletRequest, false);

        final var token = userService.register(user, password, ip);
        return ResponseEntity.ok(token);
    }


    @PostMapping("/server/login/{user}")
    public ResponseEntity<String> login(@PathVariable("user") String user, @RequestBody String password, HttpServletRequest httpServletRequest) throws IOException {

        final String ip = Utils.getIp(httpServletRequest, false);

        final String token = userService.login(user, password, ip);

        if (token == null) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
        }

        return ResponseEntity.ok(token);
    }

    @DeleteMapping(value = "/server/entry/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @SecuredRequest
    public Entry get(@PathParam("id") String id) throws IOException {

        final var user = userService.getCurrentUser();

        return dataService.getById(user.getName(), user.getPassword(), id);
    }

    @GetMapping(value = "/server/get", produces = MediaType.APPLICATION_JSON_VALUE)
    @SecuredRequest
    public List<Entry> getAll() {
        return Collections.EMPTY_LIST;
    }

    @PostMapping(value = "/server/upsert", produces = MediaType.APPLICATION_JSON_VALUE)
    @SecuredRequest
    public boolean upsert(@User String user, @RequestBody Entry entry) {

        log.info("user: {}\n", user, entry);

        return true;
    }

    @DeleteMapping(value = "/server/delete/{id}/{version}", produces = MediaType.APPLICATION_JSON_VALUE)
    @SecuredRequest
    public boolean delete(@PathParam("id") String id, @PathParam("version") int version) {
        return false;
    }
}
