# VueJS

Build on vuejs veriosn 3.

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

Serving from another port, change the `package.json`:

```
scripts.serve: "node_modules/.bin/vue-cli-service serve --port 8090" 
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

## Style SASS

`npm install --save-dev node-sass sass-loader`

```
<style lang="scss" scoped>
  ...
</style>
```

## Imported npm libs

- https://fontawesome.com/v6/docs/web/use-with/vue/

## Colorscheme

https://www.shutterstock.com/de/blog/101-farbekombinationen-design-inspiration

<font color="#E1DC9D">#E1DC9D</font>  
<font color="#8F8681">#8F8681</font>  
<font color="#A67F78">#A67F78</font>  
<font color="#32435F">#32435F</font>

## vue.config.js

```
module.exports = {
  devServer: {
    proxy: {
      '^/api': {
         target: 'http://localhost:8080',
         changeOrigin: true
      },
    }
  }
}
```

## Links

- https://medium.com/bb-tutorials-and-thoughts/development-environment-setup-for-vue-js-with-java-backend-fea45f601dc8
- https://phoenixnap.com/kb/install-npm-mac
- https://blog.codecentric.de/en/2018/04/spring-boot-vuejs/

## Icon

The icon is taken from https://icons8.com/.  
The icon was shown as free for use and the page informing about [license](https://icons8.com/license) showed
`This page could not be found` on 2022-05-29T17:23 London time. 
