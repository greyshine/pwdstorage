import axios from 'axios';

export default {

    hw() {
        console.log('hallowelt');
    },

    request(url, handler, errorHandler) {

        console.log('ajax call', url, handler, errorHandler);

        axios.get(url)
            .then(
                response => {
                    //console.log('handling response data', response);
                    handler(response);
                },
                error => {
                    if (typeof errorHandler != 'function') {
                        console.error(error)
                    }
                    errorHandler(error);
                }
            ).catch(exception => {
            console.error('exception', exception);
        });

    },

    mapDatas(datas) {

        var results = [];

        datas.forEach(d => {
            results.push(this.mapData(d));
        })

        //console.log('mapDatas->', results);
        return results;
    },

    mapData(entry) {
        return {
            id: entry.id,
            h: entry.key,
            v: entry.value,
            c: entry.created,
            u: entry.updated
        };
    },

    getTestData() {

        let d = [];

        const data1 = {
            id: 1,
            h: 'Entry 1',
            v: 'value 1',
            c: '2022-21-21 12:32:32',
            u: '2022-21-21 12:32:32'
        };

        const data2 = {
            id: 2,
            h: 'Entry 2',
            v: 'value 2',
            c: '2022-21-21 12:32:32',
            u: '2022-21-21 12:32:32'
        };

        d.push(data1);
        d.push(data2);
        d.push(data2);
        d.push(data2);
        d.push(data2);
        d.push(data2);

        return d;
    }

}