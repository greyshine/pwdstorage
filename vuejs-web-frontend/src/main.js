import {createApp} from 'vue'
import {createRouter, createWebHistory} from 'vue-router';
import App from './App.vue'
import MainPage from './MainPage.vue'
import AboutPage from './AboutPage.vue'
import FailPage from './FailPage.vue'
import mitt from 'mitt'
import axios from 'axios'

const routes = [
    {path: '/', component: MainPage},
    {path: '/about', component: AboutPage},
    {path: '/404', component: FailPage},
    {path: '/:pathMatch(.*)*', redirect: '/404'},
];

const router = createRouter({
    // 4. Provide the history implementation to use. We are using the hash history for simplicity here.
    history: createWebHistory(),
    routes: routes, // short for `routes: routes`
});


const app = createApp(App);
app.use(router);
app.config.globalProperties.$emitter = mitt();
app.config.globalProperties.$views = {};


axios.interceptors.request.use(
    config => {

        //console.log('axios.interceptor.request.$token', app.config.globalProperties.$token);

        //const token = app.config.globalProperties.$token;
        const token = app.config.globalProperties.$views.app.token;
        console.log('interceptors.request token', token);

        if (token != null) {
            config.headers.TKN = token;
        }

        config.headers['Content-Type'] = 'application/json';

        return config;
    });

axios.get('/api/version')
    .then(
        (response) => {
            //console.log('response', response);
            app.config.globalProperties.version = response.data;
        },
        (error) => {
            console.log('error', error);
        });

app.mount('#app');