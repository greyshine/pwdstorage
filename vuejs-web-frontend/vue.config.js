const {defineConfig} = require('@vue/cli-service')
module.exports = defineConfig({

  transpileDependencies: true,

  css: {
    loaderOptions: {
      sass: {
        additionalData: `@import '@/assets/scss/global.scss';`
      }
    }
  },

  devServer: {
    proxy: {
        '/api': {
            target: 'http://localhost:8080/',
            changeOrigin: true
        },
    }
  }

})
